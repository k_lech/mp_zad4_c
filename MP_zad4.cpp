﻿/*
Zaprojektować i zaimplementować klasę osoba opisaną imieniem, nazwiskiem, wiekiem i
adresem. Składowa adres powinna być dynamicznym obiektem typu adres. Klasa adres powinna
zawierać składowe miasto, kod pocztowy, ulicę oraz numer budynku. Obie klasy powinny być
wyposażone w konstruktor domyślny, konstruktor pozwalający zainicjować wszystkie pola składowe
oraz operator wstawiania do strumienia.
*/
#include "pch.h"
#include <iostream>
#include <string>
using namespace std;

class adres {
	string miasto;
	string kod_pocztowy;
	string ulica;
	int numer;

public:
	adres() : miasto("brak"), kod_pocztowy("brak"), ulica("brak"), numer(0) {}		
	adres(string _miasto, string _kod_pocztowy, string _ulica, int _numer) : miasto(_miasto), kod_pocztowy(_kod_pocztowy), ulica(_ulica), numer(_numer)	{}
	
	friend ostream & operator << (ostream & out, const adres & wyswietl);
};

ostream & operator << (ostream & out, const adres & wyswietl)
{
	return out << wyswietl.miasto << "\t" << wyswietl.kod_pocztowy << "\t" << wyswietl.ulica << "\t" << wyswietl.numer << endl;
}

class osoba {
	string imie;
	string nazwisko;
	int wiek;
	adres lokalizacja;

public:
	osoba () : imie("brak"), nazwisko("brak"), wiek(0), lokalizacja() {}		
	osoba(string _imie, string _nazwisko, int _wiek, adres _lokalizacja) : imie(_imie), nazwisko(_nazwisko), wiek(_wiek), lokalizacja(_lokalizacja)	{}
	  
	friend ostream & operator << (ostream & out, const osoba & wyswietl);
};

ostream & operator << (ostream & out, const osoba & wyswietl)
{
	return out << wyswietl.imie << "\t" << wyswietl.nazwisko << "\t" << wyswietl.wiek << "\t" << wyswietl.lokalizacja << endl;
}

int main()
{
	adres* wsk = new adres("Czestochowa", "42-200", "Dabrowskiego", 73);
	cout << *wsk << '\n';
	adres a1(*wsk);		
	delete wsk;
	const adres* wsk1 = new adres("Warszawa", "00-950", "Mysliwiecka", 357);
	cout << a1 << '\n';
	cout << *wsk1 << '\n';
	adres a2;			
	cout << a2 << '\n';
	a2 = a1;
	cout << a2 << '\n';
	
	osoba o("Jan", "Kos", 25, *wsk1);
	delete wsk1;
	cout << o << '\n';
	osoba o1(o);
	cout << o1 << '\n';
	osoba o2;
	cout << o2 << '\n';
	o2 = o1;
	cout << o2 << '\n';

	system("Pause");
	return 0;
}


